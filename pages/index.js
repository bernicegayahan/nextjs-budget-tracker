import {Row, Col} from 'react-bootstrap'
import View from '../components/View';
import Navbar from '../components/NavBar'
import AppHelper from '../app-helper.js';
import Banner from '../components/Banner'

/* const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false }) 
  */



export default function Home() {

   const data = {
    title: "Welcome to BeeTee App!",
    content: "See where your money goes",
    destination: "/",
    label: " Try it now!"
  }  
 
  return (
    <View title={'BeeTee App '}>
    {/*   <ParticlesBg type="cobweb" bg={true} /> */}
     <Navbar /> 
     <Banner />
      <Row className="justify-content-center">
        <Col xs md="6">
         
        </Col>
      </Row>
    </View>
  )
}


