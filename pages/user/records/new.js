import { useState } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import View from '../../../components/View'
import Swal from 'sweetalert2'
import AppHelper from '../../../app-helper'
import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })

const newRecord = () => {
    return (
        <View title="BeeTee App | New Record">
            <ParticlesBg type="cobweb" bg={true} />
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>New Record</h3>
                        <Card>
                            <Card.Header>Record Information</Card.Header>
                            <Card.Body>
                                <NewRecordForm />
                            </Card.Body>
                        </Card>
                </Col>
             </Row>
        </View>
    )
}
export default newRecord;

const NewRecordForm = (newRec) => {
    const [categoryName, setCategoryName] = useState(undefined)
    const [typeName, setTypeName] = useState(undefined)
    const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState("")
    const [categories, setCategories] = useState([])

    const getCategories = (value) => {
        setTypeName(value)

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({ typeName: value })
        }

        //let's send a request to backend
        fetch(`https://hidden-shore-48244.herokuapp.com/api/users/get-categories`, payload)
            .then(AppHelper.toJSON)
            .then(data => {
                setCategories(data)
                console.log(data)
                /*  if (data === true) {
                     Swal.fire('Transaction Added', 'You have successfully recorded a new transaction!', 'success')
                     Router.push('/records')
                 } else {
                     Swal.fire('Operation Failed', 'Uh oh :( Something went wrong.', 'error')
                 } */
            })
    }
    const createRecord = (create) => {
        create.preventDefault()

        const payload2 = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                typeName: typeName,
                amount: amount,
                description: description
            })
        }
        fetch(`https://hidden-shore-48244.herokuapp.com/api/users/add-record`, payload2)
            .then(AppHelper.toJSON)
            .then(newRecordData => {
                console.log(newRecordData)
                if (newRecordData === true) {
                    Swal.fire('Transaction Added', 'Your new transaction was successfully recorded!', 'success')
                    Router.push('/user/records')
                } else {
                    Swal.fire('Oh dear :(', ' Your transaction was not added, please try again.', 'error')
                }
            })
    }


    return (
        <Form variant="dark" onSubmit={(create) => createRecord(create)}>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={categoryName} onChange={(e) => getCategories(e.target.value)} required>
                    <option value selected disabled>Select Category</option>
                    <option key="a" value="Income">Income</option>
                    <option key="b" value="Expense">Expense</option>
                </Form.Control>
            </Form.Group> 
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control as="select" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} required>
                    <option value selected disabled>Select Category Name</option>{
                        categories.map((category) => {
                            return (
                                <option key={category.id} value={category.name}>
                                    {category.name}
                                </option>
                            )
                        })
                    }
                </Form.Control>
            </Form.Group>

            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control type="number" placeholder="Enter amount" onChange={(create) => setAmount(create.target.value)} value={amount}
                required />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control type="text" placeholder="Enter description" onChange={(create) => setDescription(create.target.value)} value={description} required />
            </Form.Group>
            <Button className="mt-3" variant="warning" type="submit" >Submit</Button>
        </Form>
    )
}
