import {  useState, useEffect } from 'react';
import View from '../../../components/View';
import { Container, Table} from 'react-bootstrap'
import Link from 'next/link';
import AppHelper from '../../../app-helper.js';
/* import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false }) */

const categoryIndex = (props) => {
    const [data, setData] = useState(null)
    useEffect(async () => {
        if (AppHelper.getAccessToken() == undefined) {
            return;
        }
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }
        }
        console.log(AppHelper.API_URL)
        const res = await fetch(`https://hidden-shore-48244.herokuapp.com/api/users/get-categories` ,payload)
        const records = await res.json()
        console.log(records)
        setData(records)     
    }, [])

    return (
        <Container className="mt-5 pt-4 mb-5">
            <View title="BeeTee App | Categories">
                {/* <ParticlesBg type="cobweb" bg={true} /> */}
                <h3>Categories</h3>
                <Link href="categories/new">
                    <a className="btn btn-warning mt-1 mb-3">Add</a>
                </Link>
                <Table striped bordered hover>
                    <thead className="dark">
                        <tr>
                            <th>Category</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data ? data.map(record => (<RecordsView data={record} />)) : <></>}
                    </tbody>
                </Table>
            </View>
        </Container>
    )
}

export default categoryIndex;

const RecordsView = ({ data }) => {
    console.log(data)

    return (
        <>
            <tr>
                <td>{data.name}</td>
                <td>{data.type}</td>
            </tr>
        </>
    )
}




