import { useState } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })

export default () => {
    return (
        <View title="BeeTee App | New Category">
            <ParticlesBg type="cobweb" bg={true} />

            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3 className="text-center">New Category</h3>
                    <Card>
                        <Card.Header>Category Information</Card.Header>
                        <Card.Body>
                            <NewCategoryForm />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewCategoryForm = () => {

    const [categoryName, setCategoryName]= useState("")
    const [typeName, setTypeName]= useState(undefined)    

    //create a function that will allow you to create a new database
    const createCategory = (e) => {
        e.preventDefault()// to avoide page redirections
        //describe the req you want to send
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                name: categoryName,
                typeName: typeName
            })
        }

        //send the request to the backend project for processing
        fetch(`https://hidden-shore-48244.herokuapp.com/api/users/add-category`, payload).
            then(AppHelper.toJSON).then(isSuccessful => {
                //describe the procedure upon receiving the response
                if (isSuccessful === true) {
                    Swal.fire('Category Added', 'You have successfully added a new category!', 'success')
                    Router.push('/user/categories')
                } else {
                    Swal.fire('Operation Failed', 'Uh oh :( Something went wrong.', 'error')
                }
            })
    }
    return (
        <Form variant="dark" onSubmit={ (e) => createCategory(e) }>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter category name" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={typeName} onChange={ (e) => setTypeName(e.target.value)} required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Button className="mt-3" variant="warning" type="submit">Submit</Button>
        </Form>
    )
}
