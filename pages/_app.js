import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import AppHelper from '../app-helper'
//lets make this documents as our provider component.
import { UserProvider } from '../UserContext';


function MyApp({ Component, pageProps }) {

  //what are the info/data will our provider give to the consumers?
  //lets declare the component/variable which will describe the state of a user.
  //we will initialize the user as an object.we will declare 2 properties for the object which will be set to null.
  const [user, setUser] = useState({
    email: null,
    //isAdmin: null
  })

  useEffect(() => {
    if (AppHelper.getAccessToken() !== null) {
      const options = {
        headers: {
          Authorization: `Bearer ${AppHelper.getAccessToken()}`
        }
      }
      fetch(`https://hidden-shore-48244.herokuapp.com/api/users/details`, options).then((response) => response.json()).then((userData) => {
        //lets create a control structure to determin the response if a user has been authenticated
        if (typeof userData.email != 'undefined') {
          setUser({ email: userData.email })
        } else {
          //if the condition was not met meaning the userData is null or undefined
          setUser({ email: null })
        }
      })
    }
  }, [user.id]
  )

  const unsetUser = () => {
    //this function will be used to clear out the contents of the local storage.
    localStorage.clear()
    setUser({ email: null })
  }

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
      <Navbar /> 
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </>
  )
}
export default MyApp
