import { useState, useContext } from 'react';
import UserContext from '../../UserContext';

import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

import View from '../../components/View';
//lets acquire our helper document
import AppHelper from '../../app-helper.js';
import Router from 'next/router'

import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })

export default function index() {
    return (
        <View title={'Booking App Login Page'}>
            <ParticlesBg type="cobweb" bg={true} />
            <Row className="justify-content-center">
                <Col xs md="6">
                    <Login />
                </Col>
            </Row>
        </View>
    )
}

//lets create a function that will send a request to authenticate the user
function Login() {
    //lets consume the values provided by the context object.
    const { user, setUser } = useContext(UserContext);

    //lets define a state for our token id
    const [tokenId, setTokenId] = useState(null)

    //lets define a state for our components
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const authenticate = (e) => {
        e.preventDefault() //to avoid page redirection.
        //attack the url address of the login endpoint

        //practice task, lets refactor the current structure of our fetch request by storing the payload inside an object.
        const laman = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }

        fetch(`https://hidden-shore-48244.herokuapp.com/api/users/login`, laman).then(AppHelper.toJSON).then(data => {
            console.log(data);
            //lets create a control structure that will determine the response to the user.
            if (typeof data.accessToken !== "undefined") {
                //next task is to save the access token inside our local storage object
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                //inside this branch we are going to give it these exact conditions upon failure in logging in.
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User Does Not Exist', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Authentication Failed', 'You may have registered using a different method, try using an alternative login method.', 'error')
                }
            }
        })
    }

    //new location for retrieve user details method.
    //lets create a function that will allow us to retrieve the information/details about the user.

    //upon retrieving the info about the user, the user has to be authenticated first.
    const retrieveUserDetails = (accessToken) => {
        //lets create an object which we will name as option. and the value it holds is the access token
        const options = {
            headers: { Authorization: `Bearer ${accessToken}` }
        } //this will serve as the payload of the request.

        //send the request together with the payload 
        fetch(`https://hidden-shore-48244.herokuapp.com/api/users/details`, options).then(AppHelper.toJSON).then(data => {
            //change the value of the user component by targeting it's state setter. 
            setUser({ id: data._id, isAdmin: data.isAdmin })
            //lets redirect the user inside the courses page.
            Router.push('/user/categories');
        })
    }

    return (
        <Container>
            <h1 className="text-center">Log In Page</h1>
            <Form onSubmit={e => authenticate(e)}>
                {/* email*/}
                <Form.Group controlId="email">
                    <Form.Label>Email:</Form.Label>
                    <Form.Control value={email} placeholder="Insert Email Here" type="email" onChange={e => setEmail(e.target.value)} required />
                </Form.Group>
                {/* password*/}
                <Form.Group controlId="password">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control value={password} placeholder="Insert Password Here" onChange={e => setPassword(e.target.value)} type="password" required />
                </Form.Group>
                <Button className="mt-3 mb-3 w-100" variant="dark" type="submit">Log In</Button>
                <p className="text-center">No account yet? Sign up <a href="/register">here.</a></p>

            </Form>
        </Container>
    )
}

