//lets practice creating components using JSX syntax
//the purpose of this componet is to be the hero section of our page.
import { Jumbotron, Container, Button } from 'react-bootstrap'//i'll be using a named export method to acquire my components from react-bootstrap
//lets acquire the bootstrap grid system

import  Link  from 'next/link';


//i will creata a function that will return the structure of my component
//for this example lets create this function in a es6 format
const Banner = () => {
    //lets declare a return scope to determine the anatomy of the element.
    //lets destructure the data prop into its properties
  
    return (
        <Jumbotron fluid className="jumbo">
           <Container className="text-center">
                <h1>Welcome to BeeTee | A Budget Tracker App</h1>
                <h3> See where your money goes</h3>
                <Link href="/login">Try it now!</Link>
            </Container>
           
        </Jumbotron>
    )
}

export default Banner;