import { createContext } from 'react'//our new task it to create a context data for our app which will describe the state of our user.

//1. lets use the contructor to create a contxt object
const UserContext = createContext();

export default UserContext;

//2. Lets acquire its provider component property which will allow the consumer components to subscribe to context changes to the values it holds/contains.
export const UserProvider = UserContext.Provider;